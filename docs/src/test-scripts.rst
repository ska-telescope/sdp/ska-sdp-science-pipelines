Test Scripts
============

Scripts that test the functionality of SDP. These are good starting point for
developing different types of scripts.

You can find a detailed example of a real-time and a batch script  at
:ref:`examples`

.. toctree::
  :maxdepth: 1
  :caption: Maintained

  test-scripts/test-receive-addresses
  test-scripts/test-mock-data
  test-scripts/test-realtime
  test-scripts/test-batch
  test-scripts/test-dask
  test-scripts/test-slurm

