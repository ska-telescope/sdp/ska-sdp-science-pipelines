Configuring the Script
======================

The vis-receive script can be configured through the following Processing Block parameters,
all of which can be omitted:

.. list-table:: ``vis-receive`` Processing Block parameters
   :header-rows: 1

   * - Name
     - Description
     - Default

   * - ``port_start``
     - The first port each receiver will open to listen for data.
     - ``21000``

   * - ``channels_per_port``
     - Number of frequency channels that will be sent to each port.
     - ``1``

   * - ``processes_per_node``
     - Number of processes to start per node, useful if CPU usage becomes a problem.
     - ``1``

   * - ``max_ports_per_node``
     - Maximum number of ports to assign to a node before allocating a new one.
     - ``null`` *

   * - ``num_nodes``
     - Number of nodes to allocate.
     - ``null`` *

   * - ``transport_protocol``
     - The transport protocol the receiver should use (``"tcp"`` or ``"udp"``).
     - ``"udp"``

   * - ``processors``
     - List of processors to start together with the receiver itself.
     - ``{"mswriter": {}}``

   * - ``pod_settings``
     - Extra per-pod settings.
     - ``[]``

   * - ``queue_connector_configuration``
     - Configuration passed to Queue Connectors. See :external+ska-sdp-lmc-queue-connector:doc:`index`.
     - ``{}``

   * - ``use_network_definition``
     - When set override the ``extraMetadata`` for  ``pod_settings`` from network definition in SDP etcd.
     - ``False``

   * - ``extra_helm_values``
     - Extra Helm values for the |vis-recv-chart|. Use with care.
     - ``{}``
   * - ``signal_display``
     - Configuration for the Signal Displays. (``None`` selects default values.)
     - ``{"metrics": ["stats"], "version": None, "image": None, "nchan_avg": 5}``

\* ``max_ports_per_node`` and ``num_nodes`` are mutually exclusive. If both are missing then ``num_nodes`` is defaulted to ``1``.
\* ``use_network_definition`` Note: when ``True`` this will override ``extraMetadata`` in ``pod_settings``.

An example set of parameters can be found `here <tests/test_data/default_pb_params.yaml>`__.

Processors
----------

The ``processors`` parameter is a dictionary with processor specifications.
The keys in the dictionary are a simple name of the processor,
while the value is a dictionary
with a container-like specification
as understood by the |vis-recv-chart|.

This script comes with built-in processor definitions,
which are defined as files under the [processors subdirectory](processors/):

- ``mswriter``: takes incoming visibility data
  and writes it into a Measurement Set.
- ``rcal``: performs gain calibration
  on incoming visibility data.
- ``signal-display-metrics-all``: takes incoming visibility data
  and calculates all available Signal Display metrics,
  which are then published to the SDP data queues. (deprecated, use configuration above)
- ``signal-display-metrics-basic``: takes incoming visibility data
  and calculates basic and fast Signal Display metrics,
  which are then published to the SDP data queues. (deprecated, use configuration above)
- ``signal-display-metrics-amplitude``: takes incoming visibility data
  and calculates Amplitude vs Frequency Signal Display metrics,
  which are then published to the SDP data queues. (deprecated, use configuration above)
- ``signal-display-metrics-phase``: takes incoming visibility data
  and calculates Phase vs Frequency Signal Display metrics,
  which are then published to the SDP data queues. (deprecated, use configuration above)

If a key in the ``processors`` dictionary
refers to one of the built-in processor definitions,
then the corresponding value is used
to override the built-in definition;
otherwise the value must fully define a processor.

Previously the ``processors`` parameter was a list
where elements were either the (string) name of a built-in processor,
or a dictionary with a full processor description.
For backwards compatibility this is still supported, but deprecated.
Simple names are translated to ``name: {}`` entries,
while full processor descriptions are translated
to ``description["name"]: description`` entries.

Examples:

- Only use built-in ``mswriter`` processor, with its default settings:

  .. code-block:: yaml

     mswriter: {}

- Use the built-in ``mswriter`` processor, with its default settings,
  plus a processor called ``my-signal-display-metrics``,
  which is described in full:

  .. code-block:: yaml

     mswriter: {}
     my-signal-display-metrics:
       name: my-signal-display-metrics
       image: artefact.skao.int/ska-sdp-qa-metric-generator
       version: 0.20.0
       command:
       - plasma-processor
       - ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics
       - --readiness-file
       - /tmp/processor_ready
       - --use-sdp-metadata
       - "False"
       - --metrics
       - all
       readinessProbe:
       - initialDelaySeconds: 5
         periodSeconds: 5
         exec:
           command:
           - cat
           - /tmp/processor_ready

- Use a different version of the built-in ``mswriter`` and ``rcal`` processors:

  .. code-block:: yaml

     mswriter:
       version: 1.2.3
     rcal:
       version: 2.3.4

- Old style ``processors`` parameter (deprecated):

  .. code-block:: yaml

     - mswriter
     - name: my-processor
       image: my-image

  translates to

  .. code-block:: yaml

     mswriter: {}
     my-processor:
       name: my-processor
       image: my-image

Chaining Processors
-------------------

Processors have the ability to be chained together.
This interface between each is a Visibility object.

Any processor can be a chainable processor, but at the level of the script
we have provided one example, the ``time-mswriter`` processor. So called
because it writes visibility data to a Measurement Set, but also calculates the
time average of the data before writing it.

  .. code-block:: yaml

    - name: "sdp-processor"
    - image: "registry.gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-integration/ska-sdp-realtime-receive-integration"
    - version: "0.3.0"
    - command:
      - "sdp-processor"
    - args:
      - "['ska_sdp_realtime_receive_integration.processors.AverageTimeProcessor','realtime.receive.processors.sdp.mswriter_processor.MSWriterProcessor']"
      - "--readiness-file"
      - "/tmp/processor_ready"
      - "--timestep 2"
      - "output.ms"
    - readinessProbe:
      file: "/tmp/processor_ready"

The ``args`` parameter contains a list of processor classes
that will be chained together. In this case the ``AverageTimeProcessor``
will be called first, followed by the ``MSWriterProcessor``.
The output of the first processor is passed to the second processor.

Pod settings
------------

The ``pod_settings`` parameter is a list of per-pod settings.
This is currently needed to perform certain actions that cannot be yet automated,
like pinning pods to specific nodes, or configuring access to the host's network interfaces.

Each element in this list, if given, contains the following parameters (all are optional):

.. list-table:: ``vis-receive`` ``pod_settings`` parameter options
   :header-rows: 1

   * - Name
     - Description
     - Default

   * - ``networkMapping.name``
     - Name of the NetworkAttachmentDefinition used to setup the pod' networking.
     - ``null``

   * - ``networkMapping.namespace``
     - The namespace where the NetworkAttachmentDefinition is defined.
     - ``null``

   * - ``networkMapping.deviceID``
     - The PCI ID of the network interface in the host machine (used with Multus).
     - ``null``

   * - ``networkMapping.ip``
     - The IP to assign to the network interface.
     - ``null``

   * - ``nodeSelector``
     - Dictionary used to select which node should run the pod.
     - ``null``
