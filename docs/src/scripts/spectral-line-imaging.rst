Spectral Line Imaging
=====================

This script deploys the :external+ska-sdp-spectral-line-imaging:doc:`index`.

If you want to test the script, we suggest you deploy SDP together with ITango.

The script is configured using a JSON configuration string, and implemented by running the SDP AssignResources command on the subarray device.

.. toctree::
   :maxdepth: 1

   spectral-line-imaging/configuration
   spectral-line-imaging/testing
   spectral-line-imaging/changelog

