.. _vis-receive:

Visibility Receive
==================

This script deploys the SDP visibility receiver
and updates the receive addresses attribute of the Processing Block with DNS-based IP addresses.

To set up SDP on your local machine using Minikube,
follow the instructions from the SDP integration repository
on :external+ska-sdp-integration:doc:`installation/minikube`.

.. toctree::
   :maxdepth: 1

   vis-receive/configuration
   vis-receive/changelog
