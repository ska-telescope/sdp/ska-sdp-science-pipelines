.. _pointing_configure:

Configuring the Script
======================

This section describes the configuration of the pointing-offset script, covering the options available
in the JSON configuration string supplied to AssignResources, the required environment variables, the helm
chart arguments, and setting receive addresses.

Configuration parameters
------------------------

The pointing-offset script can be configured using the parameters described at
:ref:`pb_params`, which can be added to the AssignResources configuration string
(see `example_configuration_string.json <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/blob/master/src/ska-sdp-script-pointing-offset/example_configuration_string.json#L114>`_).
Below, we describe some of the important points to note about these parameters.

The pointing-offset helm chart requires command arguments (``args``) and environment variables (``env``).
The command arguments define the options passed to the pointing pipeline.

The basic set of mandatory command arguments used by the script consists of:

  - ``eb_id`` is the execution block ID passed to the script by the Processing Block
  - ``num_scans`` is the expected number of pointing scans with MS data to be processed,
    defined by the ``num_scans`` configuration parameter (with default value of 5)

Additional command line arguments can be provided by setting the ``additional_args``
parameter in the Processing Block configuration parameters. This must be a list of strings, e.g.:
``["--num_chunks", "1", "--thresh_width", "1.25"]``. See the
:external+ska-sdp-wflow-pointing-offset:ref:`cli` for a full list of options. The additional
arguments add to (or override) the basic mandatory set.

Configuring path to Measurement Sets
....................................

The calibration pipeline loads data from the directory defined by
the vis-receive data product flow pointing to the written Measurement Set data.
The pointing-offset script points the pipeline to this data flow by retrieving
the processing block ID of vis-receive.

If there is a vis-receive script running, then the ``receive_pb_id`` needs to match
the vis-receive script PB, since the MS data will be written into that script's associated
directory. If no vis-receive script is running, then the pointing-offset script will
configure the pointing pipeline to load MS data from its own processing block directory.
The PB ID of the vis-receive script is passed to the pointing-offset script via
the ``dependencies`` argument of the Processing Block.

Telescope model data and supplying an RFI mask
..............................................

You may specify the data you want the pointing pipeline to use from the
`Telescope Model Data Repository <https://gitlab.com/ska-telescope/ska-telmodel-data>`_
by specifying the sources and file path needed. Currently only loading static RFI masks
is supported, which can be set up in the configuration string as the following example shows:

.. code-block:: javascript

   "parameters": {
       "telescope_model": {
           "telmodel_sources": ["gitlab://gitlab.com/ska-telescope/ska-telmodel-data?yan-1618-add-rfi-model#tmdata"],
           "static_rfi_key": "instrument/ska1_mid/static-rfi/rfi_mask.h5"
       }
   }


In addition, set the ``"--apply_mask"`` additional argument to actually use the mask:

.. code-block:: javascript

        "additional_args": ["--apply_mask", "1"]

Minimum allowed pointing pipeline version
.........................................

When developing with different versions of the pointing offset pipeline, ``version`` needs to be at least the
minimum allowable version, which can be found in the pointing_offset.py script as ``MIN_PIPELINE_VERSION``.
If the version is incompatible, it is reset to a default version provided by the ``DEFAULT_PIPELINE_VERSION`` variable.

Environment variables
---------------------

The following default set of environment variables are added to the pointing-offset
helm chart ``env`` parameter by the processing script:

.. list-table:: Default set of environment variables used by the pointing-offset script.
   :header-rows: 1

   * - Name
     - Description
     - Default

   * - ``SDP_PB_ID``
     - Processing block ID (get from SDP)
     - ``None``

   * - ``SDP_PROCESSING_SCRIPT``
     - Tells the pointing pipeline that it is run as an SDP script
     - ``True``

   * - ``SDP_CONFIG_HOST``
     - Host address of the Configuration DB
     - ``127.0.0.1``

   * - ``SDP_CONFIG_PORT``
     - Port of the Configuration DB
     - ``2379``

   * - ``SDP_KAFKA_TOPIC``
     - Kafka topic to send pointing offsets to
     - ``pointing_offset``

   * - ``SDP_KAFKA_SERVER``
     - Kafka server (host)
     - ``localhost:9092``

   * - ``SKA_TELMODEL_SOURCES``
     - | A list of telescope model sources as provided
       | by the ``telmodel_sources`` configuration parameter
     - ``[]``

``SDP_PROCESSING_SCRIPT`` always needs to be "True", otherwise the pointing pipeline will not check the
Configuration DB for scan information, instead it will run in a stand-alone mode, assuming the user
provides input data manually.

``SDP_CONFIG_HOST`` and ``SDP_CONFIG_PORT`` are needed for establishing a connection
to the Configuration DB and are specified directly by SDP when the script is run inside SDP.

``SDP_KAFKA_TOPIC`` is updated using the ``kafka_topic``
Processing Block configuration parameter if it exists, or defaults to the values in the table.
``SDP_KAFKA_SERVER`` is based on the ``KAFKA_HOST`` environment variable passed to the script
by the SDP Processing Controller at runtime.

You may specify additional environment variables in the configuration string ``env`` parameter.
These user defined environment variables are included with the default set and passed to the helm chart.
The optional environment variables are:

- ``LOG_LEVEL``: logging level, set to ``INFO`` by default
- ``WATCHER_TIMEOUT``: timeout in seconds for the pipeline when waiting for pointing scans,
  set to ``60`` by default
- ``POINTING_METADATA_FILE``: name of the
  :external+ska-sdp-wflow-pointing-offset:ref:`pointing metadata yaml file <metadata_yaml>`,
  set to ``ska-data-product.yaml`` by default

For example, to lower the logging level, set the following Processing Block configuration parameter:

.. code-block:: javascript

   "env": [
       {
           "name": "LOG_LEVEL",
           "value": "WARN"
       }
   ]


Script Helm chart
-----------------

The execution engine for the pointing offset calibration pipeline is the
:external+ska-sdp-helmdeploy-charts:doc:`pointing-offset chart <charts/pointing-offset>`.
For more customizable chart variables, follow the link to the documentation.

Configuring receive addresses
-----------------------------

The pointing-offset script is expected to provide the pointing offset
Fully Qualified Domain Name (FQDN) in the receiveAddresses tango attribute
of the SDP subarray. The script itself writes the information to its
processing block state, and the subarray device takes this information and
combines it with the host and port values generated by the vis-receive
script, which is then stored in the tango attribute.

The pointing FQDN is stored in the ``pointing_cal`` key defined by
:external+ska-telmodel:ref:`schema 0.5 <0.5#/definitions/beam_receive_addresses_0.5>`
(and above) of receive addresses. It is available for every scan_type
but only for "visibility"-type beams.


.. _data_flows:

Configuring data flows
----------------------

`ADR-81 <https://confluence.skatelescope.org/display/SWSI/ADR-81+Centralise+SDP+data+product%2C+queue+and+Tango+configuration>`_
describes how data flows within the SDP system. Each flow is configured by specifying
its sources and sinks, i.e. where the raw data comes from, what needs to process it
and where the processed data goes to at different stages.

The pointing-offset script configures three different data flow objects. Two of these are
used by the pointing offset calibration pipeline, one is used by the QueueConnector device.

Flows configuring the pointing calibration pipeline:

- data-product: describes where the pointing calibration results stored in HDF5 format need to be saved on storage
- data-queue: describes what Kafka queue the output offsets need to be sent to

Flow configuring the QueueConnector device:

- tango-flow: describes how to distribute incoming pointing calibration results on dish-specific tango attributes
  of the QueueConnector device.

Each flow points to another flow object as its source. E.g. the "data" flows use the
MeasurementSet(s) written by vis-receive and the source of these is loaded from the
data-product flow of vis-receive. The source of the "tango-flow" is the data sent to the
Kafka queue, i.e. the "data-queue" flow.

When the script finishes running, the data flow objects (including their states) are removed
from the Configuration Database.

.. _pb_params:

Processing block parameters
---------------------------

.. autopydantic_settings:: pointing_offset_params.PointingOffsetParams
.. autopydantic_settings:: pointing_offset_params.DataProductStorage
.. autopydantic_settings:: pointing_offset_params.TelescopeModel
.. autopydantic_settings:: pointing_offset_params.Env
