Scripts
=======

Processing scripts for SDP.

.. toctree::
  :maxdepth: 1
  :caption: Maintained

  scripts/vis-receive
  scripts/pointing-offset
  scripts/spectral-line-imaging
