"""
<SCRIPT-NAME> script
"""

import logging

import ska_sdp_scripting
import ska_ser_logging
from script_params import ScriptParams
from ska_sdp_scripting.utils import ProcessingBlockStatus

ska_ser_logging.configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


def main(pb):
    """Run processing script."""
    pb.validate_parameters(model=ScriptParams)

    # Get parameters from processing block.
    parameters = pb.get_parameters()
    duration = parameters.get("duration", 3600.0)

    # pylint: disable-next=fixme
    # TODO: Do something with the parameters if needed
    parameters["buffer"] = duration * 6e15 / 3600

    # Create work phase
    LOG.info("Creating work phase")
    work_phase = pb.create_phase("Work", [])

    with work_phase:
        LOG.info("Deploying <SCRIPT-NAME> execution engine.")

        # the following will try to deploy helm chart with the
        # hane "<SCRIPT-NAME>".
        # pylint: disable-next=fixme
        # TODO: Make sure you know what chart
        #  you want to deploy and update accordingly.
        # parameters are used to construct the chart's values.yaml file
        work_phase.ee_deploy_helm("<SCRIPT-NAME>", parameters)

        # Signal that the script is ready to do its processing
        work_phase.update_pb_state(status=ProcessingBlockStatus.READY)

        LOG.info("Done, now idling...")

        work_phase.wait_loop(work_phase.is_eb_finished)


if __name__ == "__main__":
    # Claim processing block
    with ska_sdp_scripting.ProcessingBlock() as processing_block:
        main(processing_block)
