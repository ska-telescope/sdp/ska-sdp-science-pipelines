"""
Pydantic model for processing script parameters.
"""

from pydantic import Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel


class ScriptParams(ParameterBaseModel):
    """
    <SCRIPT-NAME> script parameters
    """

    model_config = ConfigDict(title="<SCRIPT-NAME>")

    # pylint: disable-next=fixme
    # TODO: replace/add the correct parameter fields
    duration: float = Field(
        default=60.0,
        title="Duration of batch processing",
        description="The duration, in seconds, that the script "
        "will simulate processing",
    )
