#!/bin/bash

function getOciImagesToBuild() {
    IMAGES_ROOT="$(pwd)/${1}"
    IMAGE_LIST=${2}

    if [[ -z IMAGE_LIST ]]; then
      IMAGE_LIST=$(ls ${IMAGES_ROOT})
    fi

    for IMAGE in ${IMAGE_LIST}; do
        if [ -d ${IMAGES_ROOT}/${IMAGE} ]; then
            if ! git diff HEAD~1 --quiet -- "${IMAGES_ROOT}/${IMAGE}"; then
              echo "${IMAGE}"
            fi
        fi
    done
}
