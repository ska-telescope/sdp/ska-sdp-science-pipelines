"""
Pydantic model for processing script parameters.
"""

from pydantic import Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel


class TestRealtimeParams(ParameterBaseModel):
    """
    test-realtime script parameters
    """

    model_config = ConfigDict(title="test-realtime")

    length: float = Field(
        default=3600.0,
        title="Time for the processing to complete",
        description="Length of time, in seconds, for the processing to complete",
    )

    simulate_failed_engine_app: bool = Field(
        default=False,
        title="Simulate an error in an execution engine application",
        description="If True, update the processing script state with an error message",
    )

    simulate_failed_engine_start: bool = Field(
        default=False,
        title="Simulate an error in starting an engine pod",
        description="If True, update the processing script state with an error message"
        "and with state=FAILED",
    )
