Changelog
---------

1.0.0
^^^^^

- Update ska-sdp-scripting to 1.0.0
  (`MR248 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/248>`__)
- Update Dockerfile to use SKA python base image
  (`MR211 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/211>`__)

0.9.0
^^^^^

- Script allows simulating errors with the
  ``simulate_failed_engine_start`` and ``simulate_failed_engine_app``
  parameters
  (`MR202 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/202>`__)
- Update ska-sdp-scripting to 0.12.0
  (`MR202 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/202>`__)
- Processing script reports internal errors in pb state
  (`MR185 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/185>`__)
- Pydantic model included in documentation
  (`MR189 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/189>`__)
- JSON parameter schema added to tmdata
  (`MR186 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/186>`__)
- Validate processing block parameters using scripting library 0.10.0
  (`MR180 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/180>`__)
- Added processing block parameter JSON schema and Pydantic model
  (`MR174 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/174>`__,
  `MR177 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/177>`__)

0.8.0
^^^^^

- Update to scripting library 0.9.0 and use ska-sdp-python base image
  (`MR166 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/166>`__)
- Set up skart for dependency updates of processing scripts
  (`MR156 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/156>`__)
- Use poetry for dependency management
  (`MR155 <https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/merge_requests/155>`__)

0.7.0
^^^^^

- Update to scripting library 0.7.0
- **BREAKING** This and newer versions are not compatible with SDP
  version < 0.21.0

0.6.0
^^^^^

- Update to scripting library 0.6.1

0.5.1
^^^^^

- Update to latest scripting library (0.5.2).

0.5.0
^^^^^

- Update to latest scripting library (0.5.0). This required the update
  of how phase.wait_loop is used in the script.

0.4.0
^^^^^

- Update scripting library to 0.4.1.
- Set status to ``READY`` when script is ready to do its (fake)
  processing.

0.3.0
^^^^^

- Port to use SDP scripting library (formerly known as the workflow
  library).

0.2.5
^^^^^

- Use dependencies from the central artefact repository and publish the
  workflow image there.

0.2.4
^^^^^

- Ported to use the latest version of workflow library (0.2.4).

0.2.3
^^^^^

- use python:3.9-slim as the base docker image
