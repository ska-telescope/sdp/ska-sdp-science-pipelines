"""Common module for vis-receive script tests."""

import yaml


def yaml_load(stream):
    """Parse the first YAML document in a stream
    and produce the corresponding Python object.

    Args:
        stream: string stream reader

    Returns:
        Any: Document parsed as plain python
    """
    return yaml.load(stream, Loader=yaml.FullLoader)


def yaml_load_test_data(filename: str) -> dict:
    """Parse the first YAML document of the given filename
    `tests/test_data/` directory.

    Args:
        filename (str): existing YAML file name

    Returns:
        Any: Document parsed as plain python
    """
    with open(f"tests/test_data/{filename}", encoding="utf-8") as file_reader:
        return yaml_load(file_reader)


def yaml_validate(document):
    """Validates input document as safe YAML using roundtrip conversion."""
    return yaml_load(yaml.safe_dump(document))
