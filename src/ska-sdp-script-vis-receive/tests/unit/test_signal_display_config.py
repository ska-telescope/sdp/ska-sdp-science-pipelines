"""Tests for vis-receive signal display configuration."""

from unittest.mock import call

import pytest_mock
import ska_sdp_config
from common import yaml_load_test_data
from mock_processing_block import MockProcessingBlock
from ska_sdp_config.entity.flow import DataQueue
from vis_receive import update_signal_display_configuration


def test_signal_display_config(
    mocker: pytest_mock.MockerFixture, default_config_cidr24
):
    """
    Unit test for the update_signal_display_configuration
    """

    mock_create_topic_with_partitions = mocker.patch(
        "vis_receive.create_topic_with_partitions"
    )
    pb = MockProcessingBlock(config=default_config_cidr24)

    parameters = pb.get_parameters()

    # Add signal_display information, and update accordingly
    parameters["signal_display"] = {"metrics": ["all"]}
    update_signal_display_configuration(pb)
    parameters = pb.get_parameters()
    assert parameters == yaml_load_test_data("signal_display_config_all.yaml")
    assert mock_create_topic_with_partitions.mock_calls == [
        call(topic="metrics-stats-01", partitions=1),
        call(topic="metrics-spectrum-01", partitions=6),
        call(topic="metrics-lagplot-01", partitions=6),
        call(topic="metrics-bandaveragedxcorr-01", partitions=6),
        call(topic="metrics-uvcoverage-01", partitions=6),
        call(topic="metrics-amplitude-01", partitions=6),
        call(topic="metrics-phase-01", partitions=6),
    ]

    phase = pb.create_phase("test", [])
    with phase:
        assert len(phase.flows) == 9


def test_signal_display_config_missing_stats(
    mocker: pytest_mock.MockerFixture, default_config_cidr24
):
    """
    Unit test for the update_signal_display_configuration, when stats isn't given
    """
    mock_create_topic_with_partitions = mocker.patch(
        "vis_receive.create_topic_with_partitions"
    )

    pb = MockProcessingBlock(config=default_config_cidr24)

    parameters = pb.get_parameters()

    # Add signal_display information, and update accordingly
    parameters["signal_display"] = {"metrics": ["spectrum", "phase"]}
    update_signal_display_configuration(pb)

    parameters = pb.get_parameters()
    assert parameters == yaml_load_test_data("signal_display_config_missing_stats.yaml")
    assert mock_create_topic_with_partitions.mock_calls == [
        call(topic="metrics-spectrum-01", partitions=6),
        call(topic="metrics-phase-01", partitions=6),
        call(topic="metrics-stats-01", partitions=1),
    ]

    phase = pb.create_phase("test", [])
    with phase:
        assert len(phase.flows) == 5


def test_signal_display_config_override_image_version(
    mocker: pytest_mock.MockerFixture, default_config_cidr24: ska_sdp_config.Config
):
    """
    Unit test for the update_signal_display_configuration, when we override the
    `image` and `version` options.
    """
    mock_create_topic_with_partitions = mocker.patch(
        "vis_receive.create_topic_with_partitions"
    )

    pb = MockProcessingBlock(config=default_config_cidr24)

    parameters = pb.get_parameters()

    # Add signal_display information, and update accordingly
    parameters["signal_display"] = {
        "metrics": ["spectrum", "phase"],
        "image": "local_image",
        "version": "local.1",
    }
    update_signal_display_configuration(pb)
    parameters = pb.get_parameters()
    assert parameters == yaml_load_test_data("signal_display_config_override.yaml")
    assert mock_create_topic_with_partitions.mock_calls == [
        call(topic="metrics-spectrum-01", partitions=6),
        call(topic="metrics-phase-01", partitions=6),
        call(topic="metrics-stats-01", partitions=1),
    ]


def test_signal_display_config_with_existing_config(
    mocker: pytest_mock.MockerFixture, default_config_cidr24
):
    """
    update_signal_display_configuration correctly updates the
    QueueConnector exchanges list when no extra information in "signal_display"
    is provided and there are previous exchanges in the list.
    """
    mock_create_topic_with_partitions = mocker.patch(
        "vis_receive.create_topic_with_partitions"
    )

    pb = MockProcessingBlock(config=default_config_cidr24)

    pb.create_data_flow(
        "name",
        "Metrics",
        DataQueue(topics="actual-pointings", host="localhost:9092", format="npy"),
    )

    update_signal_display_configuration(pb)

    phase = pb.create_phase("test", [])
    with phase:
        assert len(phase.flows) == 4
        assert len(phase.flows[0].flow.sources) == 0

    assert mock_create_topic_with_partitions.mock_calls == [
        call(topic="metrics-stats-01", partitions=1)
    ]
