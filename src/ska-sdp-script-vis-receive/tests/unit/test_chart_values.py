"""Test module for chart value definitions."""

import pytest
from common import yaml_load_test_data
from models.vis_receive_chart_values import ReceiveProcessorValues, VisReceiveValues
from pydantic import TypeAdapter
from vis_receive import read_receiver_chart_values


def test_default_vis_receive_chart_values():
    """Test default VisReceiveValues are model compliant."""
    assert isinstance(
        TypeAdapter(VisReceiveValues).validate_python(
            yaml_load_test_data("default_script_values.yaml")
        ),
        VisReceiveValues,
    )


@pytest.mark.parametrize(
    "processor_name",
    ["mswriter", "rcal", "averagetime-mswriter", "signal-display-metrics-basic"],
)
def test_builtin_vis_receive_processor_values(processor_name: str):
    """Test processor chart values."""
    assert isinstance(
        read_receiver_chart_values(processor_name, {}), ReceiveProcessorValues
    )
