"""
Helper to perform required operations around the /network-definition stored
in the SDP configuration DB.
"""

# pylint: disable=invalid-name

from __future__ import annotations

import itertools
import json
import logging
from ipaddress import IPv4Network
from typing import Generator, Iterable

import sdp_config_resource_allocation as resource_allocation
import sdp_config_resources as resources
from models.vis_receive_script_params import VisReceiveParams
from pydantic.dataclasses import dataclass
from ska_sdp_config.config import Config, Transaction
from ska_sdp_scripting import ProcessingBlock

# Initialise logging
log = logging.getLogger(__name__)


NETWORK_RESOURCE_TYPE = "network"
NETWORK_RESOURCE_NAME = "vis-receive"
NETWORK_ATTACH_DEF_RESOURCE_TYPE = "network-attachment-definition"
SUPERNET_RESOURCE_TYPE = "supernet"


def resource_aware_txn(
    txns: Iterable[Transaction],
) -> Generator[Transaction, None, None]:
    """
    Injects ``resource`` and ``resource_allocation`` operations on generated
    Transactions
    """

    for txn in txns:
        base_txn = txn.script._txn
        txn.resource = resources.ResourceOperations(base_txn)
        txn.resource_allocation = resource_allocation.ResourceAllocationOperations(
            base_txn
        )
        yield txn


@dataclass
class SubnetAllocation:
    """A subnetwork allocation"""

    network: IPv4Network
    """The (sub)network covered by this allocation."""

    @staticmethod
    def from_network(network: str) -> SubnetAllocator:
        """
        Create a SubnetAllocation from a network string
        """
        return SubnetAllocation(network=IPv4Network(network))

    @staticmethod
    def from_config_db(value: dict) -> SubnetAllocation:
        """
        Create a SubnetAllocation from a value read from the SDP config DB
        """
        return SubnetAllocation(network=IPv4Network(value["network"]))

    def to_config_db(self) -> dict:
        """Create the value that will be stored in the SDP config DB"""
        return {"network": self.network.exploded}


@dataclass
class NetworkAttachDefResource:
    """
    A Kubernetes NetworkAtttachmentDefinition that vis-receive can set on the
    receiver pods it spawns so they can listen for external traffic.
    """

    name: str
    """Name of the NetworkAttachmentDefinition to be used by receiver pods."""

    namespace: str
    """Namespace where the NetworkAttachmentDefinition should be found."""

    @staticmethod
    def from_config_db(value: dict) -> NetworkAttachDefResource:
        """
        Create a NetworkAttachmentDefinitionResource from a value read from the
        SDP config DB.
        """
        return NetworkAttachDefResource(
            name=value["name"], namespace=value["namespace"]
        )


@dataclass
class SupernetResource:
    """Supernet from where subnets will be allocated for vis-receive pods."""

    ipv4_network: IPv4Network
    """The super network from where subnetworks will be allocated."""

    subnet_cidr_bits: int
    """Number of bits to use when allocating subnetworks."""

    last_allocated_subnet: IPv4Network | None
    """The last subnetwork that has been allocated."""

    @staticmethod
    def from_config_db(value: dict) -> SupernetResource:
        """Create a Supernet from a value read from the SDP config DB"""
        last_allocated_subnet = value.get("last-allocated-subnet")
        return SupernetResource(
            ipv4_network=IPv4Network(value["ip-supernet"]),
            subnet_cidr_bits=value["ip-subnet-cidr-bits"],
            last_allocated_subnet=(
                IPv4Network(last_allocated_subnet) if last_allocated_subnet else None
            ),
        )

    def to_config_db(self) -> dict:
        """Create the value that will be stored in the SDP config DB"""
        last_allocated_subnet = self.last_allocated_subnet
        return {
            "ip-supernet": str(self.ipv4_network),
            "ip-subnet-cidr-bits": self.subnet_cidr_bits,
            "last-allocated-subnet": (
                str(last_allocated_subnet) if last_allocated_subnet else None
            ),
        }


@dataclass
class NetworkResources:
    """
    All required network resources to perform receiver
    subnet allocation and pod configuration.
    """

    network_attach_def: NetworkAttachDefResource
    supernet: SupernetResource
    supernet_key: str


def _first_of(txn: Transaction, resource_type: str, as_type):
    results = [
        [key, as_type.from_config_db(resource)]
        for key, resource in txn.resource.list_values(resource_type=resource_type)
    ]
    if len(results) == 0:
        return None
    if len(results) > 1:
        log.warning(
            "%d %s resources found, returning first",
            len(results),
            resource_type,
        )
    return results[0]


class SubnetAllocator:
    """
    Allocates and deallocates a subnet.

    The subnet will be determined by loading in /network-definition from
    the SDP config DB then using that data to generate the next subnet.
    The subnet will then be marked as allocated in the SDP config DB.

    The subnet will be deallocated on exitting this object so best practice
    would be usage along the lines of

        with SubnetAllocator.from_pb(pb) as subnet_allocator:
            <your code here>
    """

    def __init__(
        self,
        pb_id: str,
        config: Config,
        perform_subnet_allocation: bool = True,
    ):
        # We set allocated_subnet here as it will only
        # be set elsewhere if needed.
        self._allocated_subnet: SubnetAllocation | None = None
        self._allocation_key: str | None = None
        self._network_resources: NetworkResources | None = None
        self._pb_id = pb_id
        self._config = config
        self._perform_subnet_allocation = perform_subnet_allocation

    @staticmethod
    def from_pb(processing_block: ProcessingBlock) -> SubnetAllocator:
        """Create SubnetAllocator from ProcessingBlock."""
        return SubnetAllocator(
            processing_block._pb_id,
            processing_block._config,
            processing_block.get_parameters().get("use_network_definition", False),
        )

    @property
    def pb_id(self) -> str:
        """
        ID of the Processing Block associated with this subnet resource
        allocation.
        """
        return self._pb_id

    def _get_network_attach_def_from_config_db(
        self, txn: Transaction
    ) -> NetworkAttachDefResource | None:
        return _first_of(
            txn, NETWORK_ATTACH_DEF_RESOURCE_TYPE, NetworkAttachDefResource
        )

    def _get_supernet_from_config_db(self, txn: Transaction) -> SupernetResource | None:
        return _first_of(txn, SUPERNET_RESOURCE_TYPE, SupernetResource)

    def _get_network_resources_from_config_db(
        self, txn: Transaction
    ) -> NetworkResources | None:
        network_attach_def = self._get_network_attach_def_from_config_db(txn)
        supernet = self._get_supernet_from_config_db(txn)
        if network_attach_def and supernet:
            network_attach_def = network_attach_def[1]
            supernet_key, supernet = supernet
            return NetworkResources(
                network_attach_def=network_attach_def,
                supernet=supernet,
                supernet_key=supernet_key,
            )
        return None

    def _allocate_next_available_subnet(self):
        # Retrieve any k8s Network Attachment Definition from SDP config DB
        for txn in resource_aware_txn(self._config.txn()):
            # This will be None if it hasn't been added to SDP config DB
            network_resources = self._get_network_resources_from_config_db(txn)
            if network_resources is not None:
                supernet = network_resources.supernet
                allocated_subnets = set(
                    SubnetAllocation.from_config_db(resource).network
                    for _key, resource in txn.resource_allocation.list_values(
                        resource_type=NETWORK_RESOURCE_TYPE
                    )
                )
                network_to_allocate = _get_next_subnet_from_range(
                    ipv4_network=supernet.ipv4_network,
                    last_allocated_subnet=supernet.last_allocated_subnet,
                    allocated_subnets=allocated_subnets,
                    allocate_subnet_size=supernet.subnet_cidr_bits,
                )
                network_resource_allocation = txn.resource_allocation(
                    pb_id=self.pb_id,
                    resource_type=NETWORK_RESOURCE_TYPE,
                    resource_name=NETWORK_RESOURCE_NAME,
                )
                network_resource_allocation.create(network_to_allocate.to_config_db())
                self._allocation_key = network_resource_allocation.key
            else:
                raise RuntimeError(
                    "Network configuration in SDP config "
                    "DB was empty or does not exist."
                )

            supernet.last_allocated_subnet = network_to_allocate.network
            txn.resource(key=network_resources.supernet_key).update(
                supernet.to_config_db()
            )

        # From
        # https://developer.skao.int/projects/ska-sdp-config/en/latest/design.html
        # we know that the txn for loop will repeat until it succeeds.
        self._allocated_subnet = network_to_allocate
        self._network_resources = network_resources

    @property
    def allocated_subnet(self) -> SubnetAllocation | None:
        """The network that has been allocated, if any."""
        return self._allocated_subnet

    @property
    def network_resources(self) -> NetworkResources | None:
        """The network resources used by this class."""
        return self._network_resources

    def _remove_own_allocation(self, txn: Transaction):
        own_allocation = txn.resource_allocation(key=self._allocation_key)
        resource = own_allocation.get()
        if not resource:
            log.warning(
                "Allocation under %s disappeared, will skip deallocation",
                self._allocation_key,
            )
            return
        allocated_subnet = SubnetAllocation.from_config_db(resource)
        if allocated_subnet != self._allocated_subnet:
            log.warning(
                "Allocation %s has changed contents since our initial "
                "allocation (original: %r, new: %r), will skip deallocation",
                self._allocation_key,
                self._allocated_subnet,
                allocated_subnet,
            )
            return
        own_allocation.delete()

    def release_allocated_subnet(self):
        """Release the allocated subnet if it exists."""
        if self._allocated_subnet is None:
            return
        assert self._allocation_key is not None
        for txn in resource_aware_txn(self._config.txn()):
            self._remove_own_allocation(txn)

    def __enter__(self):
        if self._perform_subnet_allocation:
            self._allocate_next_available_subnet()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release_allocated_subnet()


def _network_in_list_of_subnets(
    ipv4_network: IPv4Network, allocated_subnets: list[IPv4Network]
) -> bool:
    for allocated_subnet in allocated_subnets:
        if ipv4_network.subnet_of(allocated_subnet) or ipv4_network.supernet_of(
            allocated_subnet
        ):
            return True
    return False


def _get_next_subnet_from_range(
    ipv4_network: IPv4Network,
    last_allocated_subnet: IPv4Network | None,
    allocated_subnets: list[IPv4Network],
    allocate_subnet_size: int,
) -> SubnetAllocation:

    required_prefix = allocate_subnet_size - ipv4_network.prefixlen

    def all_subnets():
        return ipv4_network.subnets(required_prefix)

    if last_allocated_subnet is not None:

        def not_past_last_allocated_subnet(subnet):
            return subnet <= last_allocated_subnet

        # Start from after the last allocated subnet and cycle back
        subnets_after_last_allocated = itertools.dropwhile(
            not_past_last_allocated_subnet, all_subnets()
        )
        subnets_until_last_allocated = itertools.takewhile(
            not_past_last_allocated_subnet, all_subnets()
        )
        subnets_to_inspect = itertools.chain(
            subnets_after_last_allocated, subnets_until_last_allocated
        )
    else:
        subnets_to_inspect = all_subnets()

    for network in subnets_to_inspect:
        if not _network_in_list_of_subnets(network, allocated_subnets):
            log.info("available network %s", network)
            return SubnetAllocation(network=network)
        log.info("Already using network %s", network)
    # if we make it here then something went wrong determining the next subnet
    raise RuntimeError(
        f"No next subnet found in {ipv4_network} with allocated subnets "
        f"{allocated_subnets} and subnet cidr prefix length {allocate_subnet_size}."
    )


def _get_hosts(
    subnet: IPv4Network, number_hosts_required, hosts_to_skip: int = 2
) -> list[str]:
    # We want to skip the first 2 IPs as .0 would
    # be the subnet and .1 would likey be a router?
    host_list = [
        host.exploded
        for host in itertools.islice(
            subnet, hosts_to_skip, hosts_to_skip + number_hosts_required
        )
    ]
    log.info("subnet %s host_list %s", subnet, host_list)
    return host_list


def _update_pb_pod_settings_with_ip(
    network_resources: NetworkResources,
    pod_settings: VisReceiveParams.PodSettings,
    custom_ip: str,
):
    ip = f"{custom_ip}/{network_resources.supernet.subnet_cidr_bits}"
    network_config = {
        "namespace": network_resources.network_attach_def.namespace,
        "name": network_resources.network_attach_def.name,
        "ips": [ip],
    }
    extraMetadata = {
        "annotations": {"k8s.v1.cni.cncf.io/networks": json.dumps([network_config])}
    }
    pod_settings.extraMetadata = extraMetadata


def add_custom_network_definitions_and_ips(
    all_pod_settings: list[VisReceiveParams.PodSettings],
    subnet_allocator: SubnetAllocator,
) -> tuple[list[str], list[VisReceiveParams.PodSettings]]:
    """Add custom network definitions and CIDR metadata to the collection of
    all pod settings.

    Returns:
      list[str]: the subnet allocated ips."""
    allocated_subnet = subnet_allocator.allocated_subnet.network
    network_resources = subnet_allocator.network_resources

    number_of_hosts = len(all_pod_settings)
    custom_ips = _get_hosts(
        subnet=allocated_subnet, number_hosts_required=number_of_hosts
    )
    for custom_ip, pod_settings in zip(custom_ips, all_pod_settings):
        _update_pb_pod_settings_with_ip(network_resources, pod_settings, custom_ip)
    return custom_ips, all_pod_settings
