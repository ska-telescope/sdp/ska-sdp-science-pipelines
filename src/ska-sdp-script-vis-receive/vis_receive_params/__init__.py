"""Script schema module."""

from models.vis_receive_script_params import VisReceiveParams

__all__ = ["VisReceiveParams"]
