"""
Script to test batch processing.
"""

import logging
import time

import ska_ser_logging
from ska_sdp_scripting import ProcessingBlock
from test_batch_params import TestBatchParams

ska_ser_logging.configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


def main(pb):
    """Run processing script."""
    pb.validate_parameters(model=TestBatchParams)

    # Get the parameters from the processing block
    parameters = pb.get_parameters()
    duration = parameters.get("duration", 60.0)

    # Make buffer request - right now this doesn't do anything, but it gives an
    # example of how resource requests will be made
    out_buffer_res = pb.request_buffer(100.0e6, tags=["images"])

    # Create work phase with the (fake) buffer request.
    work_phase = pb.create_phase("Work", [out_buffer_res])

    # Define the function to be executed by the execution engine. In a real
    # pipeline this would be defined elsewhere and imported here.

    def some_processing(dur):
        """Do some processing for the required duration"""
        time.sleep(dur)

    with work_phase:
        deploy = work_phase.ee_deploy_test(
            "test_batch", func=some_processing, f_args=(duration,)
        )

        work_phase.wait_loop(deploy.is_finished)


if __name__ == "__main__":
    with ProcessingBlock() as processing_block:
        main(processing_block)
