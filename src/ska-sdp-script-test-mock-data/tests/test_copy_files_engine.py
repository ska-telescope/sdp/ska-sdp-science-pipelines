"""
Unittests for copy_files.py
"""

import datetime
import logging
import os
import tempfile
import time
from threading import Thread

import ska_ser_logging
from generate_mock_data.copy_files_engine import _wait_for_scan_id, _write_file
from utils import EB_ID

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_mock_data")
LOG.setLevel(logging.DEBUG)


def test_write_file_with_file(monkeypatch):
    """Unit test for write_file with filepath input"""

    with tempfile.TemporaryDirectory() as temp_dir:

        monkeypatch.setattr(
            "generate_mock_data.copy_files_engine.MOUNT_POINT", temp_dir
        )

        template_ms = "template.ms"
        os.makedirs(f"{temp_dir}/{template_ms}", exist_ok=True)
        with tempfile.NamedTemporaryFile(
            dir=f"{temp_dir}/{template_ms}", suffix=".dat"
        ) as data_file:

            results_files_ms = f"{temp_dir}/output.ms"
            _write_file(template_ms, results_files_ms)

            assert os.path.exists(results_files_ms)
            assert os.path.exists(
                f"{results_files_ms}/{os.path.basename(data_file.name)}"
            )


def test_wait_for_scan_id(default_config):
    """
    The eb's "scan_id" is None, we update the scan_id
    value to 3 after 1 second (in a thread)
    at that point the watcher iterates and finds the new scan_id
    and returns True.
    """

    def update_after_delay():
        time.sleep(1)

        new_eb_state = {
            "scan_id": 3,
            "scan_type": "pointing",
            "scans": [{"scan_id": 2, "scan_type": "pointing", "status": "FINISHED"}],
            "status": "ACTIVE",
        }

        for txn in default_config.txn():
            txn.execution_block.state(EB_ID).update(new_eb_state)

    # Set up thread to update key after delay
    thread = Thread(target=update_after_delay)
    start = datetime.datetime.now()
    thread.start()

    last_id = 2
    scan_id = _wait_for_scan_id(default_config, EB_ID, last_id)

    # Make sure to wait for thread to finish
    thread.join()
    finish = datetime.datetime.now() - start

    assert scan_id == 3

    # the update happens 1s after start
    assert finish.total_seconds() >= 1
