"""
Unittests for pointing_engine.py
"""

import os
import tempfile
from unittest.mock import patch

import numpy
import pytest
import xarray
from generate_mock_data.pointing_engine import (
    HDF_TEMPLATE,
    _convert_to_structured,
    _get_results_file_name,
    get_template_pointing_table,
    write_pointing_file,
)
from utils import RECEPTORS, SOURCE_DATA, MockPointingTable


# pylint: disable=unused-argument
def mock_export_pointingtable_to_hdf5(pointing_table, results_file_hdf, **DATA_COMMENT):
    """Mock export pointing table
    Note: DATA_COMMENT needs to be used
    in order for export_pointing_table function to work"""

    with open(f"{results_file_hdf}", "w", encoding="utf8") as file:
        file.write(f"{pointing_table.pointing.data[0, 0, 0, 0, :]}\n")
        file.write(f"{pointing_table.time.data[0]}")


@patch(
    "generate_mock_data.pointing_engine.export_pointingtable_to_hdf5",
    new=mock_export_pointingtable_to_hdf5,
)
@patch("generate_mock_data.pointing_engine.get_template_pointing_table")
def test_write_pointing_file(get_template):
    """Test function that writes the pointing HDF files"""

    get_template.return_value = (MockPointingTable(1), {})
    scan_ids = [[1, 2, 3, 4, 5], [6, 7, 9, 10, 11]]

    # Check discrete offset is consistent with number of scans
    with tempfile.TemporaryDirectory() as result_dir:
        # Loop over number of sources here
        for finished_ids in scan_ids:
            results_file_hdf = (
                f"{result_dir}/"
                f"scan{finished_ids[0]}-{finished_ids[-1]}/"
                f"pointing_offsets_scans_{finished_ids[0]}-{finished_ids[-1]}.hdf5"
            )
            os.makedirs(
                os.path.dirname(results_file_hdf),
                exist_ok=True,
            )

            write_pointing_file(
                SOURCE_DATA,
                results_file_hdf,
            )

            assert os.path.exists(results_file_hdf)

            with open(results_file_hdf, "r", encoding="utf8") as file:
                result_offsets = file.readline()
                result_time = file.readline()

            assert result_offsets == "[0. 0.]\n"
            assert result_time == f"{370.0}"


def test_convert_to_structured():
    """
    Test for _convert_to_structured.
    """

    result = _convert_to_structured(SOURCE_DATA, 5)
    assert result.shape == (len(RECEPTORS),)
    for ind, ant in enumerate(RECEPTORS):
        assert result[ind]["antenna_name"] == ant
        assert tuple(result[ind])[2:14] == tuple(numpy.zeros(12))
        assert result[ind]["last_scan_index"] == 5


def test_get_template_pointing_table_file_found(template_pointingtable):
    """Unit test for function that reads the template HDF5 file"""
    pt, data_desc = get_template_pointing_table(HDF_TEMPLATE)
    xarray.testing.assert_identical(template_pointingtable, pt)

    assert (
        data_desc["frequency"]
        == "The central frequency in Hz if fitting to gains and frequency at the "
        "higher end of the band if fitting to visibilities"
    )
    assert (
        data_desc["time"]
        == "The middle timestamp in MJD of the central scan of the pointing "
        "observation. This corresponds to the time at which the "
        "commanded_pointing is calculated. If the central scan is not found then "
        "commanded_pointing cannot be calculated and the median of the middle "
        "timestamps from all scans is used. In the two-dish mode scenario, this "
        "timestamp for the two sets of observations are stored"
    )
    assert (
        data_desc["pointing"]
        == "The pointing offsets in cross-elevation and  elevation in radians for all "
        "antennas in units of radians"
    )
    assert (
        data_desc["weight"]
        == "The inverse square of the standard error in the fitted pointing values in "
        "radians"
    )


def test_get_template_pointing_table_file_notfound():
    """Unit test for function that reads the template HDF5 file
    for the scenario where the file cannot be found"""
    with pytest.raises(ValueError):
        get_template_pointing_table("/data/template.hdf5")


@pytest.mark.skip(reason="Need Google API to make this work")
def test_get_template_pointing_table_file_from_url(template_pointingtable):
    """Unit test for function that reads the template HDF5 file
    for the scenario where the file is pulled for a remote storage"""
    pt, data_desc = get_template_pointing_table(
        "https://drive.google.com/drive/u/1/folders/1VtjGHSqE0sgeAWEvu6n2C77uueItEerQ"
    )
    xarray.testing.assert_identical(template_pointingtable, pt)

    assert (
        data_desc["frequency"]
        == "The central frequency in Hz if fitting to gains and frequency at the "
        "higher end of the band if fitting to visibilities"
    )
    assert (
        data_desc["time"]
        == "The middle timestamp in MJD of the central scan of the pointing "
        "observation. This corresponds to the time at which the commanded_pointing "
        "is calculated. If the central scan is not found then commanded_pointing "
        "cannot be calculated and the median of the middle timestamps from all scans "
        "is used. In the two-dish mode scenario, this timestamp for the two sets of "
        "observations are stored"
    )
    assert (
        data_desc["pointing"]
        == "The pointing offsets in cross-elevation and  elevation in radians for all "
        "antennas in units of radians"
    )
    assert (
        data_desc["weight"]
        == "The inverse square of the standard error in the fitted pointing values "
        "in radians"
    )


def test_get_results_file_name():
    """Unit test for get results file name"""

    with tempfile.TemporaryDirectory() as temp_dir:

        finished_ids = [2, 4, 6, 8, 9]
        results_file_hdf = _get_results_file_name(temp_dir, finished_ids)

        assert os.path.isdir(f"{temp_dir}/scan2-9")

        expected_filename = f"{temp_dir}/scan2-9/pointing_offsets_scans_2-9.hdf5"

        assert results_file_hdf == expected_filename
