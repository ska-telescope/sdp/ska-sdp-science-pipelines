"""
Unit tests for the mock data script
"""

import logging

from generate_mock_data.test_mock_data import (
    _ms_scenario,
    _pointing_scenario,
    get_image,
    get_receptors,
    set_up_params,
)
from generate_mock_data.test_mock_data_params import DEFAULT_KAFKA_TOPIC
from ska_ser_logging import configure_logging
from utils import EB_ID, KAFKA_HOST, PB_ID, RECEPTORS, SCRIPT_IMAGE

configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


def test_get_image(mock_pb):
    """Processing script image correctly obtained from config DB."""
    result = get_image(mock_pb)
    assert result == SCRIPT_IMAGE


def test_get_receptors(mock_pb):
    """Receptors are correctly obtained from EB entry"""
    result = get_receptors(mock_pb)
    assert result == RECEPTORS


def test_set_up_params():
    """Test a few values from setting up defaults in parameters"""
    parameters = {}
    set_up_params(parameters)

    assert "dataProductStorage" in parameters
    assert "env" in parameters

    assert parameters["dataProductStorage"]["mountPath"] == "/mnt/data"
    assert parameters["env"][0]["name"] == "SDP_CONFIG_HOST"
    assert parameters["env"][0]["value"] == "127.0.0.1"


def test_pointing_scenario():
    """Test the write hdf parameter setup"""

    parameters = {"kafka_topic": DEFAULT_KAFKA_TOPIC}

    _pointing_scenario(parameters, EB_ID, PB_ID, RECEPTORS)

    expected_args = [
        "pointing_engine.py",
        EB_ID,
        PB_ID,
        ",".join(RECEPTORS),
        KAFKA_HOST,
        DEFAULT_KAFKA_TOPIC,
        "both",
    ]

    assert parameters["args"] == expected_args


def test_ms_scenario():
    """Test the write ms parameter setup"""

    input_data = ["/test_path/test_file1.ms", "/test_path/test_file2.ms"]
    parameters = {"input_data": input_data}

    _ms_scenario(parameters, EB_ID, PB_ID)

    expected_args = [
        "copy_files_engine.py",
        EB_ID,
        PB_ID,
        ",".join(input_data),
    ]

    assert parameters["args"] == expected_args
