"""
Utilities for sending pointing offset data to Kafka
"""

import logging

import numpy
import ska_ser_logging
from ska_sdp_dataqueues import DataQueueProducer
from ska_sdp_dataqueues.schemas import PointingNumpyArray

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_mock_data")
LOG.setLevel(logging.INFO)

DATA_STRUCTURE = numpy.dtype(
    [
        ("antenna_name", "U8"),
        ("last_scan_index", "f8"),
        ("xel_offset", "f8"),
        ("xel_offset_std", "f8"),
        ("el_offset", "f8"),
        ("el_offset_std", "f8"),
        ("expected_width_h", "f8"),
        ("expected_width_v", "f8"),
        ("fitted_width_h", "f8"),
        ("fitted_width_h_std", "f8"),
        ("fitted_width_v", "f8"),
        ("fitted_width_v_std", "f8"),
        ("fitted_height", "f8"),
        ("fitted_height_std", "f8"),
    ]
)


def convert_to_structured(offset_data, last_scan_id):
    """
    Converts to structure array to send to Kafka.

    :param offset_data: Offsets as numpy column_data
    :param last_scan_id: ID of last scan
    """
    nants = offset_data.shape[0]

    structured_data = numpy.zeros(0, dtype=DATA_STRUCTURE)
    for ant in range(nants):
        ant_data = numpy.insert(offset_data[ant], 1, last_scan_id)
        structured_data = numpy.append(
            structured_data,
            numpy.array(tuple(ant_data), dtype=DATA_STRUCTURE),
        )
    return structured_data


async def send_data_via_kafka_pointing(kafka_host, kafka_topic, data):
    """
    Send data to Kafka in the right format.
    This one currently deals with mock pointing data.

    :param kafka_host: Kafka server address
    :param kafka_topic: Kafka topic to send to
    :param data: Data to send
    """
    LOG.debug("Sending the following data to kafka: %s", data)
    producer = DataQueueProducer(topic=kafka_topic, server=kafka_host)

    # Send the data, only numpy schema allowed currently
    async with producer:
        await producer.send(
            data,
            "msgpack_numpy",
            schema=PointingNumpyArray,
            validate=True,
        )

    LOG.info("Data is sent successfully.")
