"""
Write basic metadata file
"""

import glob
import logging
import os

import numpy
import ska_ser_logging
from ska_sdp_datamodels.calibration import import_pointingtable_from_hdf5
from ska_sdp_dataproduct_metadata import MetaData, ObsCore

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_mock_data")
LOG.setLevel(logging.INFO)

METADATA_FILE = "ska-data-product.yaml"
LIGHTSPEED = 299792458.0


def create_metadata(path, pb_id=None):
    """
    Create metadata file with basic obscore data and
    dataproduct file information to match a list of dataproduct
    files.

    :param path: full path to related dataproduct files
    """

    metadata_file_path = f"{path}/{METADATA_FILE}"

    if os.path.exists(metadata_file_path):
        metadata = MetaData(path=metadata_file_path)
        metadata.output_path = metadata_file_path
    else:
        metadata = MetaData()
        metadata.output_path = metadata_file_path

        # load basic data from processing block
        # If pb_id is None, it reads it from the
        # "SDP_PB_ID" environment variable
        metadata.load_processing_block(pb_id)

        data = metadata.get_data()

        # Common obscore items
        data.obscore.facility_name = ObsCore.SKA

        try:
            metadata.write()
        except MetaData.ValidationError as err:
            LOG.error("Validation failed with error(s): %s", err.errors)
            raise err

    # Add associated files with descriptions
    file_list = _find_files(path)
    _add_metadata_files(metadata, file_list)

    LOG.info("Metadata file updated: %s", metadata.output_path)

    return metadata


def update_detailed_pointing_metadata(metadata, file_hdf):
    """
    Updates the metadata object with obscore values.

    :param metadata: metadata to update
    :param file_hdf: path and filename to HDF file
    """

    obscore_to_fill = {}
    data = metadata.get_data()

    # Add basic obscore data
    data.obscore.instrument_name = ObsCore.SKA_MID
    data.obscore.dataproduct_type = ObsCore.DataProductType.POINTING
    data.obscore.calib_level = ObsCore.CalibrationLevel.LEVEL_0
    data.obscore.obs_collection = (
        f"{ObsCore.SKA}/"
        f"{ObsCore.SKA_MID}/"
        f"{ObsCore.DataProductType.POINTING.value}"
    )
    data.obscore.access_format = ObsCore.AccessFormat.HDF5

    # Add observation specific obscore data
    pointing_table = import_pointingtable_from_hdf5(file_hdf)

    # Catch error from old HDF files which might have missing parameters
    try:
        n_scans = int(pointing_table.scan_mode.split("-")[0])
        discrete_offset = pointing_table.attrs["discrete_offset"]
        track_duration = pointing_table.attrs["track_duration"]
    except AttributeError as err:
        n_scans = 5
        discrete_offset = numpy.zeros([2, 2])
        track_duration = 0.0
        LOG.warning(
            "Could not read parameters from HDF for calculating t_min/max. "
            "Will set both to the same value: %s",
            err,
        )

    # Find central scan (for time calculation)
    central_scan = int(n_scans / 2)
    for scan_idx, coord in enumerate(discrete_offset):
        if numpy.array_equal(coord, numpy.zeros(2)):
            central_scan = scan_idx + 1
            break

    if os.path.isfile(file_hdf):
        hdf_filesize_kb = round(os.path.getsize(file_hdf) / 1024.0)
    else:
        LOG.warning(
            "Pointing output HDF file, %s, does not exist. Setting the "
            "file size to zero in the metadata yaml file.",
            file_hdf,
        )
        hdf_filesize_kb = 0

    obscore_to_fill = {
        "obs_id": "",
        "access_estsize": hdf_filesize_kb,
        "target_name": "mock_source",
        "s_ra": pointing_table.attrs["pointingcentre"].ra.deg,
        "s_dec": pointing_table.attrs["pointingcentre"].dec.deg,
        "t_min": (
            pointing_table.time.data[0]
            - (central_scan - 1) * track_duration
            - track_duration / 2
        ),
        "t_max": (
            pointing_table.time.data[0]
            + (n_scans - central_scan) * track_duration
            + track_duration / 2
        ),
        "em_min": LIGHTSPEED / (pointing_table.frequency.data[0]),
        "em_max": LIGHTSPEED / (pointing_table.frequency.data[-1]),
        "pol_states": pointing_table.receptor_frame.type,
        "pol_xel": pointing_table.receptor_frame.nrec,
    }

    for obscore_item in obscore_to_fill.items():
        setattr(data.obscore, obscore_item[0], obscore_item[1])

    try:
        metadata.write()
    except MetaData.ValidationError as err:
        LOG.warning("Metadata validation failed with error(s): %s", err.errors)

    return metadata


def _find_files(path):
    """
    Get list of files returned as list of dictionaries
    with name and description
    """

    found_files = []

    # Check for files at path location
    files = glob.glob(f"{path}/*.*")

    for file in files:
        if os.path.basename(file) != METADATA_FILE:
            found_files.append(
                {
                    "name": file,
                    "description": "Unknown file",
                }
            )
            LOG.debug("Found file, %s", file)

    return found_files


def _add_metadata_files(metadata, file_list):
    """
    Updates the metadata to include the related files
    and sets them to done.

    :param metadata: metadata object to update
    :param file_list: list of dictionaries with filenames and descriptions
       of related dataproduct files
    """

    data = metadata.get_data()

    existing_files = [file.path for file in data.files]

    for file in file_list:
        # need to save the file path without the mount directory
        # it needs to start with /product
        if file["name"].find("product") != -1:
            file_path_wo_mount = f"/product{file['name'].split('product')[1]}"
        else:
            file_path_wo_mount = file["name"]

        if file_path_wo_mount not in existing_files:
            metadata_file = metadata.new_file(
                dp_path=file_path_wo_mount,
                description=file["description"],
            )

            # Mark file as done
            try:
                metadata_file.update_status("done")
            except MetaData.ValidationError as err:
                LOG.error("Metadata validation failed with error(s): %s", err.errors)

            LOG.info("Added associated file, %s to metadata", file["name"])
