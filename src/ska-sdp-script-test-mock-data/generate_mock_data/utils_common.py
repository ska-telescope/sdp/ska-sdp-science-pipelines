"""
Common util functions that are used with multiple scenarios.
"""

import csv
import logging
import os

import numpy
import ska_ser_logging
from ska_sdp_config.entity.common import PVCPath
from ska_sdp_config.entity.flow import (
    DataProduct,
    DataQueue,
    TangoAttribute,
    TangoAttributeMap,
    TangoAttributeUrl,
)
from test_mock_data_params import (
    DEFAULT_KAFKA_TOPIC,
    KAFKA_HOST,
    KUBE_NAMESPACE_SDP,
    MOUNT_POINT,
    SDP_DATA_PVC_NAME,
    WATCHER_TIMEOUT,
)

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_mock_data")
LOG.setLevel(logging.INFO)

# Environment parameters specific for the CSV file
INPUT_CSV_FILE = "./data/pointing_offsets.csv"
LEN_HEADER = 4
HEADINGS_ROW = 7
POINTING_SCANS = 5
NUM_SOURCES = 50


def create_result_dir(eb_id, pb_id, scan_ids=None):
    """
    This method creates the results directory
    using the mount_path, EB, PB and scan IDs
    """

    result_dir = f"{MOUNT_POINT}/product/{eb_id}/ska-sdp/{pb_id}/"

    if scan_ids:
        first_scan_id = scan_ids[0]
        last_scan_id = scan_ids[-1]

        # Add the scan ids
        result_dir = os.path.join(
            result_dir,
            f"scan{first_scan_id}-{last_scan_id}",
        )

    os.makedirs(result_dir, exist_ok=True)

    return result_dir


def _match_antennas_get_index(column_names, receptors):
    """
    Get the indices of data from the CSV file matching the
    user provided receptors
    """
    indices = numpy.array(
        [
            idx
            for antenna in receptors
            for idx, name in enumerate(column_names[0])
            if name.startswith(f"{antenna}")
        ]
    )
    if indices.size == 0:
        raise ValueError("No matching antennas found")

    return indices.reshape(len(receptors), -1)


def create_data_flows(pb, parameters, ants=None):
    """
    Create DataQueue and DataProduct objects in the configuration database

    :param pb: Processing block
    :param parameters: Parameters from the processing block
    :param ants: Antenna names from the execution block
    """
    kafka_topic = parameters.get("kafka_topic", DEFAULT_KAFKA_TOPIC)
    flow_name = "mock-data"

    queue = DataQueue(
        topics=kafka_topic,
        host=KAFKA_HOST,
        format="msgpack_numpy",
    )
    product = DataProduct(
        data_dir=PVCPath(
            k8s_namespaces=[KUBE_NAMESPACE_SDP],
            k8s_pvc_name=SDP_DATA_PVC_NAME,
            pvc_mount_path=MOUNT_POINT,
            pvc_subpath=f"product/{pb._eb_id}/ska-sdp/{pb._pb_id}",
        ),
        paths=["{common_prefix}pointing_offsets.hdf5"],
    )

    tango_product = TangoAttributeMap(
        attributes=[
            (
                TangoAttribute(
                    attribute_url=TangoAttributeUrl(
                        # only attribute name is used by QC, but we need to provide
                        # a full tango attribute url, so we default to mid-sdp. This
                        # is not used by the QC, so it doesn't matter what it's set to
                        f"tango://mid-sdp/queueconnector/01/pointing_offset_{antenna}"
                    ),
                    dtype="DevDouble",
                    max_dim_x=3,
                    default_value=float("NaN"),
                ),
                TangoAttributeMap.DataQuery(
                    select=f"[?[0]=='{antenna}'][[1], [2], [4]][]"
                ),
            )
            for antenna in ants
        ]
    )
    data_model = "PointingNumpyArray"

    kafka_flow = pb.create_data_flow(name=flow_name, data_model=data_model, sink=queue)
    pb.create_data_flow(name=flow_name, data_model=data_model, sink=product)
    # Flow to create Tango attributes from one Kafka topic
    pb.create_data_flow(
        name="tango-flow", data_model=data_model, sink=tango_product
    ).add_source(kafka_flow, function="ska-sdp-lmc-queue-connector:exchange")

    LOG.info("Created data queue and data product flows in the configuration database.")


def construct_pointing_data(source_idx, receptors):
    """Read CSV data for one source"""
    header = {}
    column_names = []
    pointing_data = {}
    commanded_az = numpy.zeros(len(receptors))
    commanded_el = numpy.zeros(len(receptors))
    offset_xel = numpy.zeros(len(receptors))
    offset_el = numpy.zeros(len(receptors))
    if not os.path.exists(INPUT_CSV_FILE):
        raise FileNotFoundError(f"Internal CSV file not found: {INPUT_CSV_FILE}")
    with open(INPUT_CSV_FILE, newline="", encoding="utf-8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=",")
        for row_idx, row_value in enumerate(csv_reader):
            if row_idx < LEN_HEADER + 1:
                # Store track_duration, band_type, scan_mode and pointing_frame
                # as keys and then their values
                header[row_value[0].split(": ")[0]] = row_value[0].split(": ")[1]
            elif row_idx == HEADINGS_ROW - 1:
                # Get the column names
                column_names.append(row_value)
            elif row_idx - HEADINGS_ROW == source_idx:
                # Extract the commanded AzEl and fitted offsets in xel and el
                # from the CSV file for those receptor names
                indices = _match_antennas_get_index(column_names, receptors)
                for i in range(len(receptors)):
                    commanded_az[i] = float(row_value[indices[i][0]])
                    commanded_el[i] = float(row_value[indices[i][1]])
                    offset_xel[i] = float(row_value[indices[i][2]])
                    offset_el[i] = float(row_value[indices[i][3]])

                pointing_data = {
                    "source_name": row_value[0],
                    "track_duration": float(header["track_duration"]),
                    "band_type": header["band_type"],
                    "scan_mode": header["scan_mode"],
                    "receptors": receptors,
                    "discrete_offset": numpy.array(
                        [
                            [0.0, -1.0],
                            [1.0, 0.0],
                            [0.0, 1.0],
                            [-1.0, 0.0],
                            [0.0, 0.0],
                        ]
                    ),
                    "ref_time": numpy.ravel(float(row_value[3])),
                    "pointing_frame": header["pointing_frame"],
                    "source_coords": (float(row_value[1]), float(row_value[2])),
                    "commanded_coords": numpy.expand_dims(
                        numpy.array([commanded_az, commanded_el]).transpose(),
                        axis=(0, 2, 3),  # 2D -> 5D
                    ),
                    "pointing_offsets": numpy.expand_dims(
                        numpy.array([offset_xel, offset_el]).transpose(), axis=(0, 2, 3)
                    ),  # 2D -> 5D
                    "frequency": numpy.array([1.355e09]),  # Band 2 central frequency
                }
    return pointing_data


def get_pointing_scan_ids(config, eb_id, last_index=-1):
    """
    Wait for a full observation consisting of any scan ids.
    Keep track of the last processed index. All we check for
    is whether the scan is finished
    """
    pointing_scans = []

    for watcher in config.watcher(timeout=WATCHER_TIMEOUT):
        for txn in watcher.txn():
            eb = txn.execution_block.state(eb_id).get()
            scans = eb.get("scans")
            scans_to_search = scans[last_index + 1 :]
            pointing_scans = [
                scan["scan_id"]
                for scan in scans_to_search
                if scan["status"] == "FINISHED"
            ]
        # If more than one batch is available
        # we process the first batch and come back
        if len(pointing_scans) >= POINTING_SCANS:
            last_index = (
                last_index + len(scans_to_search) - len(pointing_scans) + POINTING_SCANS
            )
            break

    return pointing_scans[:POINTING_SCANS], last_index
