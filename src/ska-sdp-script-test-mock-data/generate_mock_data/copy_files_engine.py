"""
Write MS file(s).
"""

import logging
import os
import shutil
import sys

import ska_ser_logging
from ska_sdp_config import Config
from utils_common import MOUNT_POINT, WATCHER_TIMEOUT, create_result_dir
from utils_metadata import create_metadata

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_mock_data")
LOG.setLevel(logging.INFO)


def _write_file(template_ms, results_files_ms):
    """
    Method to write one MS from input path
    """
    if template_ms == "":
        LOG.warning("Cannot write output: template not specified")
        return

    if os.path.isdir(f"{MOUNT_POINT}/{template_ms}"):
        shutil.copytree(
            f"{MOUNT_POINT}/{template_ms}",
            results_files_ms,
            copy_function=shutil.copy,
            dirs_exist_ok=True,
        )
        LOG.info("MS files written: %s", results_files_ms)

    else:
        LOG.warning("Cannot write output: template not found, %s", template_ms)


# pylint: disable-next=inconsistent-return-statements
def _wait_for_scan_id(config, eb_id, last_scan_id=-1):
    """
    Get the scan ID for a new running scan (i.e. different
    to the last found scan ID)
    """
    for watcher in config.watcher(timeout=WATCHER_TIMEOUT):
        for txn in watcher.txn():
            eb = txn.execution_block.state(eb_id).get()
            scan_id = eb.get("scan_id")
            if scan_id is not None and scan_id != last_scan_id:
                return scan_id


def copy_files(args):
    """
    Main function to write MS files
    Currently, it only supports copying.
    """
    eb_id = args[0]
    pb_id = args[1]
    msfiles = args[2]

    if msfiles == "":
        msfiles = []
        result_dir = ""
        LOG.warning("No input files were specified!")
    else:
        msfiles = msfiles.split(",")
        result_dir = create_result_dir(eb_id, pb_id)

    config = Config()

    last_scan = -1
    for msfile in msfiles:
        LOG.info("Waiting for scan to start...")
        scan_id = _wait_for_scan_id(config, eb_id, last_scan)
        LOG.info("Processing scan %s", scan_id)

        results_files_ms = f"{result_dir}/scan-{scan_id}.ms"

        _write_file(msfile, results_files_ms)

        # Create/update associated metadata file
        create_metadata(result_dir, pb_id=pb_id)

        last_scan = scan_id

    config.close()
    LOG.info("Processed all requested scans. Now idling.")
    while True:
        pass


if __name__ == "__main__":
    argv = sys.argv[1:]
    copy_files(argv)
