# pylint: disable=too-many-locals

"""
Pointing Engine file
"""

import asyncio
import logging
import os
import sys

import h5py
import numpy
import ska_ser_logging
from astropy.coordinates import SkyCoord
from ska_sdp_config import Config
from ska_sdp_datamodels.calibration import (
    PointingTable,
    convert_hdf_to_pointingtable,
    export_pointingtable_to_hdf5,
)
from ska_sdp_datamodels.configuration.config_create import Configuration
from utils_common import (
    NUM_SOURCES,
    POINTING_SCANS,
    construct_pointing_data,
    create_result_dir,
    get_pointing_scan_ids,
)
from utils_kafka import convert_to_structured, send_data_via_kafka_pointing
from utils_metadata import create_metadata, update_detailed_pointing_metadata

ska_ser_logging.configure_logging()
LOG = logging.getLogger("test_mock_data")
LOG.setLevel(logging.INFO)

HDF_TEMPLATE = "./data/template.hdf5"


def _convert_to_structured(source_data, last_scan_id):
    """
    Convert data from interval CSV file to structured array
    for Kafka.

    :param source_data: Dictionary used to update template file
    :param last_scan_id: Last observed scan id
    """

    values = numpy.zeros((len(source_data["receptors"]), 12))
    values[:, 0] = numpy.radians(source_data["pointing_offsets"][0, :, 0, 0, 0])
    values[:, 2] = numpy.radians(source_data["pointing_offsets"][0, :, 0, 0, 1])
    data = numpy.column_stack((source_data["receptors"], values))

    structured_data = convert_to_structured(data, last_scan_id)

    return structured_data


def _get_results_file_name(result_dir, finished_ids):
    """Construct the output filename"""

    # Output filename
    results_file_hdf = (
        f"{result_dir}/"
        f"scan{finished_ids[0]}-{finished_ids[-1]}/"
        f"pointing_offsets_scans_{finished_ids[0]}-{finished_ids[-1]}.hdf5"
    )
    os.makedirs(
        os.path.dirname(results_file_hdf),
        exist_ok=True,
    )

    return results_file_hdf


def get_template_pointing_table(template_file):
    """Read template pointing table"""

    if os.path.exists(template_file):
        pointing_table, data_descriptions = get_pointingtable_with_data_desc(
            template_file
        )
    else:
        raise ValueError(f"Template file, {template_file}, cannot be found")

    return (pointing_table, data_descriptions)


def get_pointingtable_with_data_desc(filename):
    """Method to import pointing table and data
    descriptions from HDF5 file"""

    data_descriptions = {}
    with h5py.File(filename, "r") as hdf_file:

        pointing_table = convert_hdf_to_pointingtable(hdf_file["PointingTable0"])

        for key, value in hdf_file["PointingTable0"].items():
            if "description" in value.attrs:
                data_descriptions[key.replace("data_", "")] = value.attrs["description"]

    return (pointing_table, data_descriptions)


def write_pointing_file(source_data, results_file_hdf):
    """
    Updates the pointing offsets in the sample HDF file with the
    supplied ones.

    :param source_data: Dictionary used to update template file
    :param results_file_hdf: Path to filename for output
    """

    LOG.info("Read template file: %s", HDF_TEMPLATE)
    pointing_table, data_descriptions = get_template_pointing_table(HDF_TEMPLATE)

    # These attributes need to be consistent so that the start/end time
    # can be calculated from the reference time
    pointing_table.attrs["band_type"] = source_data["band_type"]
    pointing_table.attrs["scan_mode"] = source_data["scan_mode"]
    pointing_table.attrs["discrete_offset"] = source_data["discrete_offset"]
    pointing_table.attrs["track_duration"] = source_data["track_duration"]

    # Copy the configuration from the template file and repeat for the number
    # of receptors. This means the dishes will be on top of each other

    config = Configuration.constructor(
        name=pointing_table.attrs["configuration"].name,
        location=pointing_table.attrs["configuration"].location,
        names=source_data["receptors"],
        xyz=pointing_table.attrs["configuration"].xyz.data.repeat(
            len(source_data["receptors"]), axis=0
        ),
        mount=pointing_table.attrs["configuration"].mount.data.repeat(
            len(source_data["receptors"])
        ),
        frame=pointing_table.attrs["configuration"].frame,
        receptor_frame=pointing_table.attrs["configuration"].receptor_frame,
        diameter=pointing_table.attrs["configuration"].diameter.data.repeat(
            len(source_data["receptors"])
        ),
        offset=pointing_table.attrs["configuration"].offset.data.repeat(
            len(source_data["receptors"]), axis=0
        ),
        stations=pointing_table.attrs["configuration"].stations.data.repeat(
            len(source_data["receptors"])
        ),
        vp_type=pointing_table.attrs["configuration"].vp_type.data.repeat(
            len(source_data["receptors"])
        ),
    )

    # Now, construct the PointingTable using the number of antennas from template.
    pointing_table = PointingTable.constructor(
        pointing=numpy.radians(source_data["pointing_offsets"]),
        time=source_data["ref_time"],
        weight=numpy.ones_like(source_data["pointing_offsets"]),
        frequency=source_data["frequency"],
        expected_width=numpy.ones_like(source_data["pointing_offsets"]),
        fitted_width=numpy.ones_like(source_data["pointing_offsets"]),
        fitted_width_std=numpy.ones_like(source_data["pointing_offsets"]),
        fitted_height=numpy.ones_like(source_data["pointing_offsets"][:, :, :, :, 0]),
        fitted_height_std=numpy.ones_like(
            source_data["pointing_offsets"][:, :, :, :, 0]
        ),
        receptor_frame=pointing_table.attrs["receptor_frame"],
        band_type=source_data["band_type"],
        scan_mode=source_data["scan_mode"],
        track_duration=source_data["track_duration"],
        discrete_offset=source_data["discrete_offset"],
        commanded_pointing=numpy.radians(source_data["commanded_coords"]),
        pointingcentre=SkyCoord(
            source_data["source_coords"][0], source_data["source_coords"][1], unit="deg"
        ),
        configuration=config,
    )

    export_pointingtable_to_hdf5(pointing_table, results_file_hdf, **data_descriptions)

    LOG.info("HDF5 file written: %s", results_file_hdf)


def pointing_scenario(args):
    """Processing scenario of"""
    eb_id = args[0]
    pb_id = args[1]
    receptors = args[2].split(",")
    kafka_host = args[3]
    kafka_topic = args[4]
    option = args[5]
    if option not in ["write-hdf", "send-to-kafka", "both"]:
        raise ValueError(f"Pointing_option: {option} is not a valid value")

    if option in ["write-hdf", "both"]:
        result_dir = create_result_dir(eb_id, pb_id)

    config = Config()

    # Use internal template and listen for any pointing scans
    # and continually loop over sources in CSV file.
    last_idx = -1
    source_idx = 0
    while source_idx < NUM_SOURCES:

        LOG.info("Waiting for %s pointing scans...", POINTING_SCANS)
        finished_ids, last_idx = get_pointing_scan_ids(
            config, eb_id, last_index=last_idx
        )
        LOG.info("Processing %s-point observation: %s", POINTING_SCANS, finished_ids)

        source_data = construct_pointing_data(source_idx, receptors)

        if option in ["write-hdf", "both"]:
            LOG.info("Writing pointing output file")
            results_file_hdf = _get_results_file_name(result_dir, finished_ids)

            # Update and write out pointing table
            write_pointing_file(source_data, results_file_hdf)

            # Create associated metadata file
            metadata = create_metadata(os.path.dirname(results_file_hdf), pb_id=pb_id)
            update_detailed_pointing_metadata(metadata, results_file_hdf)

        if option in ["send-to-kafka", "both"]:

            # Convert data to structured numpy array before sending to kafka
            data = _convert_to_structured(source_data, finished_ids[-1])
            LOG.info("Sending data to Kafka: \n%s", data)
            asyncio.run(send_data_via_kafka_pointing(kafka_host, kafka_topic, data))

        source_idx = (source_idx + 1) % NUM_SOURCES

    config.close()
    LOG.info("Processed all requested scans. Now idling.")
    while True:
        pass


if __name__ == "__main__":
    argv = sys.argv[1:]
    pointing_scenario(argv)
