"""
Pydantic model for processing script parameters.
"""

import os
from typing import Literal

from pydantic import Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel

# Environment variables for the script
KAFKA_HOST = os.getenv("SDP_KAFKA_HOST", "localhost:9092")
WATCHER_TIMEOUT = int(os.getenv("WATCHER_TIMEOUT", "60"))
SDP_DATA_PVC_NAME = os.getenv("SDP_DATA_PVC_NAME")
KUBE_NAMESPACE_SDP = os.getenv("SDP_HELM_NAMESPACE")
MOUNT_POINT = "/mnt/data"
DEFAULT_KAFKA_TOPIC = "pointing_offset"


class TestMockDataParams(ParameterBaseModel):
    """
    test-mock-data script parameters
    """

    model_config = ConfigDict(title="test-mock-data")

    kafka_topic: str = Field(
        default=DEFAULT_KAFKA_TOPIC,
        title="Kafka topic",
        description="Kafka topic name. If not supplied, the topic will be set to "
        f"'{DEFAULT_KAFKA_TOPIC}'.",
    )

    input_data: list | None = Field(
        default=None,
        title="List of Measurement Sets",
        description="List of Measurement Sets to use in the measurement-set "
        "scenario - e.g. ['path/scan-1.ms', 'path/scan-2.ms'].",
    )

    scenario: Literal[
        "pointing",
        "measurement-set",
        None,
    ] = Field(
        default=None,
        title="Scenario name",
        description="The name of the mock data scenario. Allowed: 'pointing' for "
        "sending pointing data to Kafka and/or writing pointing HDF files, "
        "'measurement-set' for copying MS to the output directory.",
    )

    pointing_option: Literal["write-hdf", "send-to-kafka", "both"] = Field(
        default="both",
        title="Flag to specify options for pointing scenario",
        description="Specifies the output of the 'pointing' scenario. Allowed: "
        "'send-to-kafka' for sending pointing data to kafka, 'write-hdf' for "
        "writing pointing HDF files, 'both' for sending pointing data to kafka and "
        "writing pointing HDF files.",
    )
