"""
Pydantic model for processing script parameters.
"""

import logging
import os
from typing import Literal

from pydantic import BaseModel, Field
from pydantic.config import ConfigDict
from ska_sdp_scripting.processing_block import ParameterBaseModel
from ska_ser_logging import configure_logging

configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)

KUBE_NAMESPACE_SDP = os.getenv("SDP_HELM_NAMESPACE")
SDP_DATA_PVC_NAME = os.getenv("SDP_DATA_PVC_NAME")

DEFAULT_PIPELINE_VERSION = "1.0.0"
DEFAULT_KAFKA_TOPIC = "pointing_offset"
DEFAULT_ENCODING = "msgpack_numpy"


class DataProductStorage(BaseModel):
    """PVC name and path to data product storage"""

    name: str = Field(
        default=SDP_DATA_PVC_NAME,
        title="PVC name",
        description="PVC name pointing to the data product storage."
        "The default value comes from the SDP_DATA_PVC_NAME "
        "environment variable.",
    )
    mountPath: str = Field(
        default="/mnt/data",
        title="Path where the PVC is mounted in the pointing pipeline container",
        description="Path where the data product storage PVC is "
        "mounted in the pointing pipeline container",
    )


class TelescopeModel(BaseModel):
    """Telescope Model Information"""

    telmodel_sources: list[str] = Field(
        default_factory=list,
        title="Telescope Model sources",
        description="Telescope model sources",
    )

    static_rfi_key: str = Field(
        default="",
        title="Static RFI key",
        description="Filename/key to static RFI mask in Telescope Model",
    )


class Env(BaseModel):
    """User-defined Environment variables"""

    name: str = Field(
        ...,
        title="Environment variable name",
        description="User-defined environment variable name",
    )
    value: str = Field(
        ...,
        title="Environment variable value",
        description="User-defined environment variable value",
    )


class PointingOffsetParams(ParameterBaseModel):
    """
    Pointing offset script parameters
    """

    model_config = ConfigDict(title="pointing-offset")

    version: str = Field(
        pattern=r"^[a-zA-Z0-9_\.-]+$",
        default=DEFAULT_PIPELINE_VERSION,
        title="Pointing offset calibration pipeline Version",
        description="Version of the pointing offset calibration pipeline used "
        "by the helm chart for deployment",
    )

    image: str = Field(
        pattern=r"^[a-zA-Z0-9_:\./-]+$",
        default="artefact.skao.int/ska-sdp-wflow-pointing-offset",
        title="OCI image of the pointing offset calibration pipeline",
        description="The OCI image used by the helm chart to launch the "
        "pointing offset calibration pipeline",
    )

    imagePullPolicy: str = Field(
        pattern=r"^(Always|IfNotPresent|Never)$",
        default="IfNotPresent",
        title="Pull policy of the image",
        description="Kubernetes image pull policy used by the helm "
        "chart (Always, IfNotPresent, Never)",
    )

    command: Literal["pointing-offset"] = Field(
        default="pointing-offset",
        title="Run command for the pointing offset calibration pipeline",
        description="The command the helm chart deploys the pointing "
        "offset pipeline with",
    )

    encoding: Literal["msgpack_numpy"] = Field(
        default=DEFAULT_ENCODING,
        title="Pointing data encoding",
        description="Encoding used by the QueueConnector "
        "to decode pointing data from Kafka",
    )

    kube_namespaces: str = Field(
        default=KUBE_NAMESPACE_SDP,
        title="Kubernetes namespace",
        description="Kubernetes namespace where the pointing offset calibration "
        "pipeline is deployed. The default is given by the "
        "SDP_HELM_NAMESPACE environment variable.",
    )

    dataProductStorage: DataProductStorage = Field(
        default_factory=DataProductStorage,
        title="Data Product storage PVC",
        description="PVC name and path to data product storage",
    )

    kafka_topic: str = Field(
        default=DEFAULT_KAFKA_TOPIC,
        title="Kafka topic",
        description="Kafka topic that the QueueConnector will use to load pointing "
        "offset data from",
    )

    telescope_model: TelescopeModel = Field(
        default_factory=TelescopeModel,
        title="Telescope Model reference",
        description="Dictionary of path and file values to specify static "
        "data for the pipeline coming from Telescope Model data",
    )

    env: list[Env] = Field(
        default_factory=list,
        title="Environment variables",
        description="User-defined environment variables",
    )

    num_scans: int = Field(
        default=5,
        title="Number of scans",
        description="Expected number of scans in a pointing observation",
    )

    additional_args: list[str] = Field(
        default=["--use_source_offset_column"],
        title="Additional arguments",
        description="Additional arguments to be passed to the pointing offset"
        " calibration pipeline",
    )

    tango_attribute: str = Field(
        default="pointing_offset",
        title="Tango attribute",
        description="Prefix to the tango attribute name which will hold the "
        "pointing offsets on the QueueConnector device. "
        "(i.e. '<tango_attribute>_{dish_id}')",
    )

    args: list[str] = Field(
        default_factory=list,
        title="Pointing offset calibration pipeline arguments",
        description="CLI arguments for the pointing offset calibration pipeline.",
    )
