"""Unit tests for the pointing offset script"""

import logging
from pathlib import PosixPath
from types import SimpleNamespace
from unittest.mock import patch

import pytest
from pointing_offset import (
    MIN_PIPELINE_VERSION,
    create_data_flows,
    find_receive_address,
    get_receive_pb_id,
    set_up_envs,
    update_parameters_for_execution,
    update_pipeline_version,
    valid_pipeline_version,
)
from pointing_offset_params import DEFAULT_PIPELINE_VERSION
from ska_sdp_config.entity.flow import Flow, FlowSource
from ska_sdp_scripting import ProcessingBlock
from ska_ser_logging import configure_logging

configure_logging()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


# Mock objects
class MockProcessingBlock(ProcessingBlock):
    """Mocked version of ska_sdp_scripting.ProcessingBlock
    overriding functions only needed in testing"""

    # pylint: disable-next=super-init-not-called
    def __init__(self, config, **kwargs):
        self._eb_id = "eb-test-20210630-00000"
        self._config = config
        self._pb_id = "pb-0-00000000-0"
        self.queue = []
        self._flows = []
        self.product = []
        self.parameters = kwargs

    def get_dependencies(self):
        """
        String set solely for test purposes
        """
        mock_dep = SimpleNamespace(pb_id="pb-flow-12345678-0")

        return [] if self._config is None else [mock_dep]

    def get_scan_types(self):
        """
        String set solely for test purposes
        """
        scan_types = [
            {
                "scan_type_id": "pointing",
                "beams": {
                    "vis0": {
                        "polarisations_id": "all",
                        "channels_id": "vis_channels",
                    }
                },
            },
        ]
        return scan_types


# pylint: disable-next=too-few-public-methods
class MockPhase:
    """Mock Phase object"""

    def __init__(
        self,
        config,
    ):
        self._eb_id = "eb-test-20210630-00000"
        self._config = config


# Helper functions
def assert_dict_value(result, expected):
    """
    Assert values in two dicts are the same
    """
    for key, value in expected.items():
        if isinstance(value, dict):
            assert_dict_value(result[key], value)
        else:
            assert result[key] == value


def test_set_up_envs():
    """
    Unit test for set_up_envs using default values
    """
    # Initialise from empty dictionary
    parameters = {}

    set_up_envs(parameters)

    # Default PVC is None
    # Other parameters are using default values
    expected_parameters = {
        "dataProductStorage": {"name": None},
        "env": [
            {"name": "SDP_PROCESSING_SCRIPT", "value": "True"},
            {"name": "SDP_CONFIG_HOST", "value": "127.0.0.1"},
            {"name": "SDP_CONFIG_PORT", "value": "2379"},
            {"name": "SDP_KAFKA_TOPIC", "value": "pointing_offset"},
            {"name": "SDP_KAFKA_SERVER", "value": "localhost:9092"},
            {"name": "SKA_TELMODEL_SOURCES", "value": ""},
        ],
    }

    assert_dict_value(parameters, expected_parameters)


def test_set_up_envs_parameters():
    """
    Unit test for set_up_envs using parameters
    """

    # Update parameters with telmodel soucres
    parameters = {
        "num_scans": "10",
        "telescope_model": {
            "telmodel_sources": [
                "gitlab://gitlab.com/ska-telescope/"
                "ska-telmodel-data?yan-1618-add-rfi-model#tmdata"
            ],
            "static_rfi_key": "instrument/ska1_mid/static-rfi/rfi_mask.h5",
        },
    }

    set_up_envs(parameters)
    expected_parameters = {
        "dataProductStorage": {"name": None},
        "env": [
            {"name": "SDP_PROCESSING_SCRIPT", "value": "True"},
            {"name": "SDP_CONFIG_HOST", "value": "127.0.0.1"},
            {"name": "SDP_CONFIG_PORT", "value": "2379"},
            {"name": "SDP_KAFKA_TOPIC", "value": "pointing_offset"},
            {"name": "SDP_KAFKA_SERVER", "value": "localhost:9092"},
            {
                "name": "SKA_TELMODEL_SOURCES",
                "value": "gitlab://gitlab.com/ska-telescope/"
                "ska-telmodel-data?yan-1618-add-rfi-model#tmdata",
            },
        ],
    }
    assert_dict_value(parameters, expected_parameters)


def test_get_receive_pb_id(default_config):
    """
    Unit test for get_receive_pb_id
    """

    test_pb_id = "000000000"

    pb = MockProcessingBlock(config=default_config)
    receive_id = get_receive_pb_id(pb, test_pb_id)
    # This returns the pb_id from dependencies
    assert receive_id == "pb-flow-12345678-0"

    pb = MockProcessingBlock(config=None)
    receive_id = get_receive_pb_id(pb, test_pb_id)
    # This returns the test_pb_od
    assert receive_id == "000000000"


@patch(
    "pointing_offset.PB_ID",
    "87654321",
)
def test_execution_parameters():
    """
    Unit test for update_parameters_for_execution
    """
    parameters = {
        "num_scans": "10",
        "telescope_model": {
            "telmodel_sources": [
                "gitlab://gitlab.com/ska-telescope/"
                "ska-telmodel-data?yan-1618-add-rfi-model#tmdata"
            ],
            "static_rfi_key": "instrument/ska1_mid/static-rfi/rfi_mask.h5",
        },
    }
    eb_id = "000000000"
    # Parameters are updated in place
    update_parameters_for_execution(parameters, eb_id)

    # num_scan is carried on
    # msdir uses receive_pb_id
    # result_dir uses PB_ID (set differently)
    expected_parameters = {
        "num_scans": "10",
        "telescope_model": {
            "telmodel_sources": [
                "gitlab://gitlab.com/ska-telescope/"
                "ska-telmodel-data?yan-1618-add-rfi-model#tmdata"
            ],
            "static_rfi_key": "instrument/ska1_mid/static-rfi/rfi_mask.h5",
        },
        "args": [
            "compute",
            "--eb_id",
            "000000000",
            "--num_scans",
            "10",
            "--use_source_offset_column",
            "--rfi_file",
            "instrument/ska1_mid/static-rfi/rfi_mask.h5",
        ],
    }

    assert_dict_value(parameters, expected_parameters)


@patch(
    "pointing_offset.PB_ID",
    "87654321",
)
def test_execution_parameters_norfi():
    """
    Unit test for update_parameters_for_execution
    When no RFI information is provided
    """
    parameters = {
        "num_scans": "10",
        "additional_args": ["--apply_mask", "--use_source_offset_column"],
    }
    eb_id = "000000000"
    # Parameters are updated in place
    update_parameters_for_execution(parameters, eb_id)

    # RFI mask parameter is removed
    expected_parameters = {
        "num_scans": "10",
        "args": [
            "compute",
            "--eb_id",
            "000000000",
            "--num_scans",
            "10",
            "--use_source_offset_column",
        ],
    }

    assert_dict_value(parameters, expected_parameters)


def test_receive_address(default_config):
    """
    Unit test for find_receive_address
    """

    pb = MockProcessingBlock(config=default_config)
    work_phase = MockPhase(config=default_config)
    recv_address = find_receive_address(work_phase, pb)

    expected_recv_address = {
        "pointing": {
            "vis0": {"pointing_cal": "tango://test/device/0/pointing_offset_{dish_id}"}
        }
    }

    assert_dict_value(recv_address, expected_recv_address)


@pytest.mark.parametrize(
    "version, valid",
    [
        (MIN_PIPELINE_VERSION, True),
        (DEFAULT_PIPELINE_VERSION, True),
        ("0.3.9", False),
        ("5.6.1", True),
        (None, False),
    ],
)
def test_valid_pipeline_version(version, valid):
    """Test pipeline version validation"""
    assert valid_pipeline_version(version) is valid


@pytest.mark.parametrize(
    "given_version, final_version",
    [
        ("0.0.0", DEFAULT_PIPELINE_VERSION),
        ("0.5.9", DEFAULT_PIPELINE_VERSION),
        ("5.6.1", "5.6.1"),
        ("1.10.0", "1.10.0"),
        ("1.1.0-dev.cd0319f38", "1.1.0-dev.cd0319f38"),
        (None, DEFAULT_PIPELINE_VERSION),
    ],
)
def test_update_to_invalid_version(given_version, final_version):
    """
    Test to update an invalid pipeline version
    """
    parameters = {"version": given_version}
    update_pipeline_version(parameters)
    assert parameters["version"] == final_version


@patch(
    "pointing_offset.SDP_DATA_PVC_NAME",
    "shared",
)
@patch("pointing_offset.KUBE_NAMESPACE_SDP", "test_namespace")
def test_create_data_flows(default_config):
    """
    Unit test for create_data_flows
    """

    pb = MockProcessingBlock(config=default_config)
    parameters = {
        "dataProductStorage": {"mountPath": "/mnt/data"},
        "kube_namespaces": [
            "control_system_namespace",
            "processing_namespace",
        ],
    }
    ants = [
        "SKA001",
        "SKA002",
        "SKA003",
    ]
    # Create 3 DataFlow flows:
    #   One a DataQueue sink relating to a Kafka topic
    #   One a DataProduct sink relating to a file in a PVC in a Kubernetes
    #   namespace
    #   One TangoAttributeMap flow to decompose a Kafka topic
    #   (which uses the Kafka flow as the flow Source)
    create_data_flows(pb, parameters, ants)

    expected_source = FlowSource(
        uri=Flow.Key(
            pb_id="pb-flow-12345678-0",
            name="vis-receive-mswriter-processor",
            kind="data-product",
        ),
        function="ska-sdp-wflow-pointing-offset:pointing-offset",
    )

    assert pb._flows[0].flow.sources[0] == expected_source
    assert pb._flows[1].flow.sources[0] == expected_source

    assert pb._flows[0].flow.sink.topics == "pointing_offset"
    assert str(pb._flows[0].flow.sink.host) == "kafka://localhost:9092"
    assert pb._flows[0].flow.sink.format == "msgpack_numpy"

    assert pb._flows[1].flow.sink.data_dir.pvc_mount_path == PosixPath("/mnt/data")
    assert pb._flows[1].flow.sink.data_dir.k8s_pvc_name == "shared"
    assert pb._flows[1].flow.sink.data_dir.k8s_namespaces == [
        "control_system_namespace",
        "processing_namespace",
    ]
    assert pb._flows[1].flow.sink.paths[0] == PosixPath(
        "{common_prefix}pointing_offsets.hdf5"
    )

    assert len(pb._flows[2].flow.sink.attributes) == 3
    assert pb._flows[2].flow.sink.attributes[0][0].kind == "tango"
    assert (
        pb._flows[2].flow.sink.attributes[0][0].attribute_url.attribute_name
        == "pointing_offset_SKA001"
    )
    assert len(pb._flows[2].flow.sources) == 1
    assert pb._flows[2].flow.sources[0].uri.kind == "data-queue"
